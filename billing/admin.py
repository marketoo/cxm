from django.contrib import admin
from billing.models import Tarif, Payment

class TarifAdmin(admin.ModelAdmin):
  list_display = ('title', 'is_active', 'tarif_period', 'period_length')
  list_filter = ['is_active', 'tarif_period']

class PaymentAdmin(admin.ModelAdmin):
  list_display = ('client', 'timestamp', 'ammount', 'is_credited')
  list_filter = ['timestamp', 'is_credited', 'client']

admin.site.register(Tarif, TarifAdmin)
admin.site.register(Payment, PaymentAdmin)
