from djmoney.models.fields import MoneyField

from django.db import models

TARIF_PERIODS = (
  ('dayly', 'Ежедневно'),
  ('monthly', 'Ежемесячно'),
  ('annualy', 'Ежегодно'),
)

class Tarif(models.Model):
  title = models.CharField(max_length=50, verbose_name='название тарифа')
  is_active = models.BooleanField(default=False, verbose_name='активен')
  description = models.TextField(default='', verbose_name='описание')
  tarif_period = models.CharField(max_length=7, choices=TARIF_PERIODS,
                                  verbose_name='период тарификации')
  period_length = models.SmallIntegerField(default=1,
                                           verbose_name='длительность тарифа')

  def __str__(self):
    return "%s%s" % (self.title, '[архив]' if not self.is_active else '')

  class Meta:
    verbose_name = 'тариф'
    verbose_name_plural = 'тарифы'



class Payment(models.Model):
  timestamp = models.DateTimeField(auto_now_add=True,
                                   verbose_name='отметка времени')
  client = models.ForeignKey(to='cxm.Client', on_delete=models.PROTECT,
                             verbose_name='оплативший клиент')
  ammount = MoneyField(max_digits=14, decimal_places=2,
                             default_currency='RUB')
  is_credited = models.BooleanField(default=False,
                                    verbose_name='Зачтен ли платеж в счет оплаты тарифа')

  class Meta:
    verbose_name = 'платеж'
    verbose_name_plural = 'платежи'



