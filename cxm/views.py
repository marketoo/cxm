import json

from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, JsonResponse, Http404

from .models import Questionary
from .forms import StarsIndexForm

def display_qr(request, qr_type, hash_code):
  qr_generator = 'https://chart.googleapis.com/chart?cht=qr&chs=300x300&choe=UTF-8&chl='
  scheme = request.is_secure() and "https" or "http"
  host = request.META['HTTP_HOST']

  url = scheme + '://' + host + '/cxm/' + qr_type + '/' + hash_code
  qr_url = qr_generator + url
  context = {
    'title': 'QR',
    'qr_url': qr_url,
    'url': url
  }
  html_template = loader.get_template('qr_code.html')
  return HttpResponse(html_template.render(context, request))

def display_questionary(request, hash_code):
  try:
    q = Questionary.objects.get(hash_code=hash_code)
  except Questionary.DoesNotExist:
    raise Http404

  questions = json.loads(q.questions)['questions']
  branch_questionary = json.loads(q.pos.client.branch.questionary)

  question_list = []
  for question in questions:
    print(question)
    question_list.append(StarsIndexForm(initial={'questionary': q.hash_code, 'index': question}))

    """TODO:
    + Скрыть поля questionare и index
    - Добавить подстановку текста вопроса в label поля со звездами
    - Добавить отличительный признак в название поля questionare и index для каждого вопроса, чтобы иметь возможность сохранять каждый ответ отдельной записью (сделать под каждый индекс отдельную форму???)
    - разобраться, как на лету сделать форму с несколькими полями (или сабмит нескольких форм сразу)
    - Добавить сабмит формы
    """

  context = {
    'title': 'Hello world!',
    'q': q,
    'form': question_list
  }
  html_template = loader.get_template('web_form.html')
  return HttpResponse(html_template.render(context, request))

def save_score(request, **score_data):
  pass

