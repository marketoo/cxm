from datetime import datetime

from django.db import models

from hashid_field import HashidAutoField
from djmoney.models.fields import MoneyField

from billing.models import Tarif
from mptt.models import MPTTModel, TreeForeignKey

INDEX_TYPES_CHOICES = (
    ('stars', 'Звездочки'),
    ('int', 'Целочисленный'),
    ('txt', 'Текстовый'),
    ('bool', 'Да/Нет'),
)



class Branch(models.Model):
  title = models.CharField(max_length=100, verbose_name='название')
  questionary = models.TextField(default='')

  def __str__(self):
    return self.title

  class Meta:
    ordering = ['title']
    verbose_name = 'отрасль'
    verbose_name_plural = 'отрасли'



class Client(models.Model):
  title = models.CharField(max_length=200, verbose_name='название')
  branch = models.ForeignKey(Branch, on_delete=models.PROTECT,
                             verbose_name='отрасль')
  tarif = models.ForeignKey(Tarif, on_delete=models.PROTECT,
                            verbose_name='тариф')
  registered = models.DateTimeField(auto_now_add=True)
  account_balance = MoneyField(max_digits=14, decimal_places=2,
                             default_currency='RUB')
  paid_till = models.DateField(null=True)

  def __str__(self):
    return self.title

  class Meta:
    verbose_name = 'клиент'
    verbose_name_plural = 'клиенты'



class POS(MPTTModel):
  title = models.CharField(max_length=100, verbose_name='название')
  client = models.ForeignKey(Client, on_delete=models.CASCADE,
                             verbose_name='клиент')
  parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True,
                          blank=True, related_name='children')

  def __str__(self):
    return self.title

  class Meta:
    verbose_name = 'точка обслуживания'
    verbose_name_plural = 'точки обслуживания'




class Index(models.Model):
  title = models.CharField(max_length=50, verbose_name='название индекса')
  code = models.CharField(max_length=20, verbose_name='код')
  index_type = models.CharField(max_length=25, choices=INDEX_TYPES_CHOICES,
                                default='stars', verbose_name='тип индекса')
  min_value = models.SmallIntegerField(default=0)
  max_value = models.SmallIntegerField(default=10)
  value_step = models.SmallIntegerField(default=2)
  description = models.TextField()

  def __str__(self):
    return self.title

  class Meta:
    verbose_name = 'оцениваемый параметр'
    verbose_name_plural = 'оцениваемые параметры'




class Questionary(models.Model):
  pos = models.ForeignKey(POS, on_delete=models.CASCADE)
  timestamp = models.DateTimeField(auto_now_add=True)
  is_active = models.BooleanField(default=False, verbose_name='активеный')
  hash_code = HashidAutoField(min_length=12, primary_key=True)
  questions = models.TextField(verbose_name='описание опросника в формате json')

  def __client(self):
    return self.pos.client

  def __str__(self):
    return "[%s | %s]" % (self.pos, self.timestamp)

  class Meta:
    verbose_name = 'опросник'
    verbose_name_plural = 'опросники'




class StarScore(models.Model):
  questionary = models.ForeignKey(Questionary, on_delete=models.CASCADE)
  index = models.ForeignKey(Index, on_delete=models.PROTECT)
  timestamp = models.DateTimeField(auto_now_add=True)
  score = models.IntegerField()

  class Meta:
    verbose_name = 'оценка звездочкой'
    verbose_name_plural = 'оценки звездочкой'

