from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from cxm.models import *

class BranchAdmin(admin.ModelAdmin):
  list_display = ('title',)

class ClientAdmin(admin.ModelAdmin):
  pass

class POSAdmin(MPTTModelAdmin):
  pass

class IndexAdmin(admin.ModelAdmin):
  list_display = ('title', 'code', )
  list_filter = ['index_type']

class QuestionaryAdmin(admin.ModelAdmin):
  list_display = ('pos', 'timestamp', 'hash_code', )
  list_filter = ['is_active']

class StarScoreAdmin(admin.ModelAdmin):
  pass

admin.site.register(Branch, BranchAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(POS, POSAdmin)
admin.site.register(Index, IndexAdmin)
admin.site.register(Questionary, QuestionaryAdmin)
admin.site.register(StarScore, StarScoreAdmin)
