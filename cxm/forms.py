from django import forms
from django_starfield import Stars
from cxm.models import StarScore

class StarsIndexForm(forms.Form):
  questionary = forms.CharField(widget = forms.HiddenInput())
  index = forms.CharField(widget = forms.HiddenInput())
  score = forms.IntegerField(widget=Stars)

  class Meta:
    model = StarScore
