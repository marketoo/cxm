# -*- encoding: utf-8 -*-

from django.urls import path, include

from cxm import views

urlpatterns = [
  path('qr/<qr_type>/<hash_code>', views.display_qr, name='display_qr'),
  path('q/<hash_code>', views.display_questionary, name='display_questionary'),
  path('s/<hash_code>', views.save_score, name='save_score'), #Q: Под каждый вариант ответа генерировать qr? тагсибе
]
